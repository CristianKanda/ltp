module Primes where

-- Con listas intensionales
isPrime :: Int -> Bool
isPrime x
    | (length xs) == 2 = True
    | otherwise = False
        where xs = [y | y<-[1..x], x `mod` y == 0]

primes :: Int -> [Int]
primes x = take x [y | y<-[1..], isPrime y == True]

-- Sin listas intesionales
isPrime' :: Int -> Bool
isPrime' x
	| (length (divisores x x)) == 2 = True
	| otherwise = False

primes' :: Int -> [Int]
primes' 0 = []
primes' x = take x (genPrim 1) 

genPrim :: Int -> [Int]
genPrim x = if isPrime x then x:(genPrim (x+1)) else genPrim (x+1)

divisores :: Int -> Int -> [Int]
divisores _ 0 = []
divisores x y
	| x `mod` y == 0 = y:(divisores x (y-1))
	| otherwise = (divisores x (y-1))